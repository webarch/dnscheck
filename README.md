This role replicates a small part of the functionality from the 
[users role](https://git.coop/webarch/users), it can be used to 
check if domain names resolve to hosts.
